<?php

/**
 * @file
 * Admin form for Yesmail.
 */

/**
 * Administrative settings for the Yesmail module.
 *
 * @ingroup forms
 */
function yesmail_settings_form($form, &$form_state) {
  $form['api_details'] = array(
    '#type' => 'fieldset',
    '#title' => t('API connection details'),
    '#description' => t('Use the following settings to connect the module with the yesmail API.'),
    '#collapsible' => TRUE,
  );
  $form['api_details']['yesmail_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('yesmail_api_key', ''),
    '#required' => TRUE,
  );
  $form['api_details']['yesmail_api_user'] = array(
    '#type' => 'textfield',
    '#title' => t('API User'),
    '#default_value' => variable_get('yesmail_api_user', ''),
    '#required' => TRUE,
  );
  $form['api_details']['yesmail_api_testing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use test API'),
    '#default_value' => variable_get('yesmail_api_testing', ''),
  );

  if (module_exists('yesmail_email')) {
    $form['yesmail_email'] = array(
      '#type' => 'fieldset',
      '#title' => t('Yesmail Email Forms'),
      '#description' => t('These settings will apply to all Yesmail Email forms.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['yesmail_email']['yesmail_email_clientside_js'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use jQuery validate for clientside validation'),
      '#default_value' => variable_get('yesmail_email_clientside_js', ''),
    );
  }

  return system_settings_form($form);
}
