<?php

/**
 * @file
 * Contains Drupal\yesmail\Exceptions\YesmailApiException.
 */

namespace Drupal\yesmail\Api;

/**
 * This does not do anything special, it's for better readability.
 *
 * @package Drupal\yesmail\Exceptions
 */
class YesmailApiException extends \Exception {}
