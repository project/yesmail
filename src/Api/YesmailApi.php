<?php

/**
 * @file
 * Contains Drupal\yesmail\Api\YesmailApi.
 */

namespace Drupal\yesmail\Api;

/**
 * Main class for performing API requests against the Yesmail v2 API.
 *
 * @package Drupal\yesmail\Api
 */
class YesmailApi extends YesmailApiBase {

  /**
   * {@inheritdoc}
   */
  public function sendEmail($email, $mmid, $transactional = false) {
    $email = strtolower($email);
    $params = [
      'transactional' => $transactional,
      'content' => ['templateId' => $mmid],
      'recipients' => [['email' => $email, 'id' => $email]]
    ];
    return $this->makePostRequest('emails/send', $params);
  }

}
