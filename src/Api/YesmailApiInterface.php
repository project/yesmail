<?php

/**
 * @file
 * Contains Drupal\yesmail\Api\YesmailApiInterface.
 */

namespace Drupal\yesmail\Api;

/**
 * Class YesmailApiBase.
 *
 * @package Drupal\yesmail\Api
 */
interface YesmailApiInterface {

  /**
   * Make a POST request to the Yesmail API.
   *
   * @param string $url
   *   The URL to make the request against.
   * @param array $body
   *   The data to send to the API.
   *
   * @return array
   *   Result of the API request.
   */
  public function makePostRequest($url, $body = array());

  /**
   * Make a GET request to the Yesmail API.
   *
   * @param string $url
   *   The URL to make the request against.
   * @param array $query_params
   *   The data to send to the API.
   *
   * @return array
   *   Result of the API request.
   */
  public function makeGetRequest($url, $query_params = array());

  /**
   * Get the absolute URL for a Yesmail API endpoint.
   *
   * @param string $endpoint
   *   The endpoint you want the full URL for.
   *
   * @return string
   *   The full endpoint URL.
   */
  public function getEndpointUrl($endpoint);

}
