<?php

/**
 * @file
 * Contains Drupal\yesmail\Api\YesmailApiBase.
 */

namespace Drupal\yesmail\Api;

use Drupal\yesmail\Config\YesmailConfigInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Message\RequestInterface;
use GuzzleHttp\Message\ResponseInterface;

/**
 * Class YesmailApiBase.
 *
 * @package Drupal\yesmail\Api
 */
abstract class YesmailApiBase implements YesmailApiInterface {

  /**
   * Construct a YesmailApiBase object.
   *
   * @param YesmailConfigInterface $config
   *    Yesmail configuration object.
   * @param ClientInterface $http_client
   *    An authenticated Guzzle instance connected to the Yesmail API.
   */
  public function __construct(YesmailConfigInterface $config, ClientInterface $http_client) {
    $this->config = $config;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function makePostRequest($url, $json = array()) {

    $request = $this->httpClient->createRequest(
      'post',
      $this->getEndpointUrl($url),
      array(
        'json' => $json,
      )
    );

    return $this->makeRequest($request, 'post');

  }

  /**
   * {@inheritdoc}
   */
  public function makeGetRequest($url, $query_params = array()) {

    $request = $this->httpClient->createRequest(
      'get',
      $this->getEndpointUrl($url),
      array(
        'query' => $query_params,
      )
    );

    return $this->makeRequest($request);
  }

  /**
   * A lower level way to submit a request to the Yesmail API.
   *
   * @param RequestInterface $request
   *    A Guzzle request instance.
   *
   * @see YesmailApiBase::makeGetRequest()
   * @see YesmailApiBase::makePostRequest()
   *
   * @return array
   *   The JSON response from the Yesmail API, or an empty array if no
   *   response was given or if the status code was incorrect.
   */
  private function makeRequest(RequestInterface $request, $method = 'GET') {
    try {
      $response = $this->httpClient->send($request);

      $this->checkStatusCode($response);

      $json = $response->json();

      return $json;
    }
    catch (YesmailApiException $e) {
      $this->logToWatchdog($e->getMessage());
      return array();
    }
    catch (\Exception $e) {
      $this->logToWatchdog($e->getMessage());
      return array();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpointUrl($endpoint) {
    $url_builder = array(
      $this->config->getFullApiUrl(),
      $this->config->getApiVersion(),
      $endpoint,
    );
    return implode('/', $url_builder);
  }

  /**
   * Check the status code of a request.
   *
   * @param ResponseInterface $response
   *   The Guzzle response object to check the status code for.
   *
   * @throws YesmailApiException
   *   Thrown when the status code is anything other than 200.
   *
   * @return int
   *   200 if the response is OK. Otherwise an exception is thrown.
   */
  private function checkStatusCode(ResponseInterface $response) {
    $status_code = $response->getStatusCode();
    switch ($status_code) {
      case 200:
      case 201:
      case 202:
      case 204:
        break;

      case 400:
      case 401:
      case 403:
      case 404:
      case 405:
      case 409:
      case 413:
      case 500:
        throw new YesmailApiException($response->getReasonPhrase());

    }
    return $status_code;
  }

  /**
   * Log an error to Drupal's watchdog.
   *
   * @param string $message
   *    The message to add.
   * @param array $variables
   *   Any replacements to pass to the error message.
   * @param int $severity
   *   The level of error to raise.
   * @param string $link
   *   A link to associate with the message.
   */
  private function logToWatchdog($message, $variables = array(), $severity = WATCHDOG_ERROR, $link = NULL) {
    watchdog('yesmail_api', $message, $variables, $severity, $link);
  }

}
