<?php

/**
 * @file
 * Contains Drupal\yesmail\Api\YesmailApiFactory.
 */

namespace Drupal\yesmail\Api;

use Drupal\yesmail\Config\YesmailConfig;
use Drupal\yesmail\Connection\YesmailConnectionFactory;

/**
 * Class YesmailApiFactory.
 *
 * @package Drupal\yesmail\Api
 */
class YesmailApiFactory {

  /**
   * Construct a YesmailAPIFactory instance.
   */
  public function __construct() {
    $this->config = new YesmailConfig();
    $http_factory = new YesmailConnectionFactory($this->config);
    $this->httpClient = $http_factory->getClient();
  }

  /**
   * Get a YesmailAPI instance.
   *
   * @return \Drupal\yesmail\Api\YesmailApi
   *   A ready to use instance of the Yesmail API.
   */
  public function get() {
    return new YesmailApi($this->config, $this->httpClient);
  }

}
