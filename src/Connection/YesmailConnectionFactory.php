<?php

/**
 * @file
 * Connection class to provide to YesmailApiBase extensions.
 */

namespace Drupal\yesmail\Connection;

use Drupal\yesmail\Config\YesmailConfigInterface;
use GuzzleHttp\Client;

/**
 * Get a fully authenticated API connection class.
 *
 * @package Drupal\yesmail\Connection
 */
class YesmailConnectionFactory implements YesmailConnectionFactoryInterface {

  /**
   * Configuration object for Yesmail.
   *
   * @var \Drupal\yesmail\Config\YesmailConfigInterface
   */
  protected $config;

  /**
   * Guzzle HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  private $httpClient;

  /**
   * Construct a YesmailConnectorFactory object.
   */
  public function __construct(YesmailConfigInterface $config) {
    $this->config = $config;
    try {
      // This is the normal Guzzle client that you use in the application.
      $this->httpClient = new Client([
        'defaults' => [
          'headers' => array(
            'Api-Key' => $this->config->getApiKey(),
            'Api-User' => $this->config->getApiUser(),
          ),
        ],
      ]);
    }
    catch (\Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getClient() {
    return $this->httpClient;
  }

}
