<?php

/**
 * @file
 * Contains YesmailConnectionFactoryInterface.
 *
 * Full path:
 *  - Drupal\yesmail\Connection\YesmailConnectionFactoryInterface.
 */

namespace Drupal\yesmail\Connection;

/**
 * Class YesmailConnector.
 */
interface YesmailConnectionFactoryInterface {

  /**
   * Get the connection instance to the established Yesmail session.
   *
   * @return \GuzzleHttp\Client
   *   A Guzzle client instance that represents the connection to Yesmail.
   */
  public function getClient();

}
