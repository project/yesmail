<?php

/**
 * @file
 * Contains \Drupal\yesmail\Config\YesmailConfigInterface.
 */

namespace Drupal\yesmail\Config;

/**
 * Class YesmailConfigInterface.
 *
 * Contains interface definition for Yesmail config classes.
 */
interface YesmailConfigInterface {

  /**
   * Get the configured API key for Yesmail.
   *
   * @return string
   *   The configured API key for Yesmail.
   */
  public function getApiKey();

  /**
   * Get the configured API user for Yesmail.
   *
   * @return string
   *   The configured API user for Yesmail.
   */
  public function getApiUser();

  /**
   * Get the API URL for Yesmail.
   *
   * @return string
   *   The API URL for Yesmail.
   */
  public function getApiUrl();

  /**
   * Get the full API URL for Yesmail.
   *
   * @return string
   *   The full API URL for Yesmail.
   */
  public function getFullApiUrl();

  /**
   * Get the currently supported version string.
   *
   * @return string
   *   The version string for the currently supported API.
   */
  public function getApiVersion();

}
