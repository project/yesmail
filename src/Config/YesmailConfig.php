<?php

/**
 * @file
 * Contains Drupal\yesmail\Config\YesmailConfig.
 */

namespace Drupal\yesmail\Config;

/**
 * Class YesmailConfig.
 *
 * Contains configuration for Yesmail that can be passed around the app.
 */
class YesmailConfig implements YesmailConfigInterface {

  private $apiKey;

  private $apiUser;

  private $apiUrl = 'api.yesmail.com';

  private $apiTestUrl = 'api.test.yesmail.com';

  private $apiVersion = 'v2';

  /**
   * Construct a YesmailConfig object.
   */
  public function __construct() {
    $this->apiKey = variable_get('yesmail_api_key');
    $this->apiUser = variable_get('yesmail_api_user');
    $this->apiTest = variable_get('yesmail_api_testing');
  }

  /**
   * {@inheritdoc}
   */
  public function getApiKey() {
    return $this->apiKey;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUser() {
    return $this->apiUser;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl() {
    return $this->apiUrl;
  }

  /**
   * {@inheritdoc}
   */
  public function getFullApiUrl() {
    $url = !empty($this->apiTest) ? $this->apiTestUrl : $this->apiUrl;
    return 'https://' . $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiVersion() {
    return $this->apiVersion;
  }

}
