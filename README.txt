CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
The Yesmail module provides integration with Yesmail's email marketing
platform.

REQUIREMENTS
------------

This module requires:

 * A Yesmail subscription
 * A Yesmail developer account (https://developer.yesmail.com/getting-started)
 * A Yesmail API key (these can be created with a developer account)

It also has the following dependencies:

 * Composer manager (http://www.drupal.org/project/composer_manager)
 * X Autoload (http://www.drupal.org/project/xautoload)

RECOMMENDED MODULES
-------------------

Email forms created with Yesmail Email have the option to use the jQuery
validation plugin (http://jqueryvalidation.org/). If jQuery plugins is
installed, this module will be the source of the validation plugin. Otherwise,
the plugin will be hotlinked using one of the Microsoft Ajax CDN files found
on the plugin homepage.

 * jQuery plugins (https://www.drupal.org/project/jquery_plugin)

INSTALLATION
------------

    1. Enable the Yesmail.

    2. Make sure composer_manager has permission to write to its library/vendor
    directory. It will need to download any libraries Yesmail depends on.

If you are using Drush, use the following commands to download the
dependencies using composer_manager:

 drush composer-rebuild-file
 drush composer-manager update

Otherwise use the Composer Manager UI (admin/config/system/composer-manager) to
update the dependencies.

The main Yesmail module will not do anything on its own. It only provides an
API. If you would like to create email forms that interact with the Yesmail
service, you can enable the Yesmail Email (yesmail_email) submodule.

CONFIGURATION
-------------

 * Global settings page: admin/config/services/yesmail
 * Email forms page: admin/config/services/yesmail/email.

Enter your Yesmail API credentials on the Global settings page. Once the
proper information is entered you will be able to interact with the API.

To use Yesmail's test API, be sure to check the "Use test API" checkbox in the
API connection details fieldset on the settings page.

If Yesmail Email has been enabled, there will be a "Email Forms" tab available
on the Global settings page as well as a "Yesmail email forms" fieldset. Within
this group there is an option to use jQuery validate for clientside validation.
The jQuery plugins contrib module must be installed and enabled for this to
function.

MAINTAINERS
-----------

Current maintainers:
 * Joe Hassick (jh3) - https://www.drupal.org/user/533048
