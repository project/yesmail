<?php

/**
 * @file
 * Displays a yesmail email form block.
 *
 * Available variables:
 * - $yesmail_email_form: The complete search form ready for print.
 * - $yesmail: Associative array of search elements. Can be used to print each
 *   form element separately.
 *
 * Default elements within $search:
 * - $yesmail['title']: Title of email form wrapped in a h2.
 * - $yesmail['description']: Description of email form wrapped in a div.
 * - $yesmail['email']: Text input area wrapped in a div.
 * - $yesmail['actions']: Rendered form buttons.
 * - $yesmail['hidden']: Hidden form elements. Used to validate forms when
 *   submitted.
 *
 * Modules can add to the search form, so it is recommended to check for their
 * existence before printing. The default keys will always exist. To check for
 * a module-provided field, use code like this:
 * @code
 *   <?php if (isset($yesmail['extra_field'])): ?>
 *     <div class="extra-field">
 *       <?php print $yesmail['extra_field']; ?>
 *     </div>
 *   <?php endif; ?>
 * @endcode
 *
 * @see template_preprocess_yesmail_email_block_form()
 */
?>
<div class="container-inline">
  <?php print $yesmail_email_form; ?>
</div>
