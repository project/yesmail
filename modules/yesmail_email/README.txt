Description
-----------------------------------------------------------------------------

A submodule of Yesmail that integrates with the Yesmail Email API.

Create blocks for triggering a Yesmail email transaction anywhere on your
Drupal site.

Requirements
-----------------------------------------------------------------------------

* Yesmail developer account using API v2
* The Yesmail module
* Entity: http://www.drupal.org/project/entity
* X Autoload: http://www.drupal.org/project/xautoload

Installation
-----------------------------------------------------------------------------

1. Enable the Yesmail Email module.

Usage
-----------------------------------------------------------------------------

To create an email form:

1. Go to admin/config/services/yesmail/email.

2. Click the "Add yesmail email form" link.

3. Complete the form.

4. Clear caches if your block does not appear on the Blocks page
(admin/structure/blocks).

More options will appear on the Global settings page
(admin/config/services/yesmail) once this module is enabled.

Form validation
-----------------------------------------------------------------------------

All email form blocks come with basic email validation. You can optionally
enable clientside validation on the email field. Visit the settings page
(admin/config/services/yesmail) and open the "Yesmail Email Forms" fieldset.
Check "Use jQuery validate for clientside validation" to enable clientside
validation.

When this is checked, the jQuery validation plugin
(http://jqueryvalidation.org) will be used one of two ways:

* Via jquery_plugin: http://drupal.org/project/jquery_plugin
* Or, a file from the Microsoft's Ajax CDN

Theming
-----------------------------------------------------------------------------

Take a look at yesmail-email-block-form.tpl.php for theming possibilities. Copy
the template file into your own theme in order to override the template file
provided by this module.
