<?php

/**
 * @file
 * The yesmail_email module admin settings.
 */

use Drupal\yesmail_email\YesmailEmail;

/**
 * Return a form for adding/editing a Yesmail email form.
 *
 * @TODO
 * Make the page display mode functional.
 */
function yesmail_email_form($form, &$form_state, YesmailEmail $email) {
  $form_state['email'] = $email;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title for this email form.'),
    '#size' => 55,
    '#maxlength' => 255,
    '#default_value' => $email->title,
    '#required' => TRUE,
  );

  // Machine-readable list name.
  $status = isset($email->status) && $email->yid && (($email->status & ENTITY_IN_CODE) || ($email->status & ENTITY_FIXED));
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $email->name,
    '#maxlength' => 32,
    '#disabled' => $status,
    '#machine_name' => array(
      'exists' => 'yesmail_email_load_multiple_by_name',
      'source' => array('title'),
    ),
    '#description' => t('A unique machine-readable name for this form. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($email->settings['description']) ? $email->settings['description'] : '',
    '#rows' => 2,
    '#maxlength' => 500,
    '#description' => t('This description will be shown with the email form. Basic HTML tags are allowed. Limited to 500 characters or less.'),
  );
  $mode_defaults = array(
    YESMAIL_EMAIL_BLOCK => array(YESMAIL_EMAIL_BLOCK),
  );
  $form['mode'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display Mode'),
    '#required' => TRUE,
    '#options' => array(
      YESMAIL_EMAIL_BLOCK => t('Block'),
    ),
    '#default_value' => !empty($email->mode) ? $mode_defaults[$email->mode] : array(),
  );

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['settings']['submit_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit Button Label'),
    '#required' => 'TRUE',
    '#default_value' => isset($email->settings['submit_button']) ? $email->settings['submit_button'] : t('Submit'),
  );

  $form['settings']['confirmation_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Confirmation Message'),
    '#description' => t('This message will appear after a successful submission of this form. Leave blank for no message.'),
    '#default_value' => isset($email->settings['confirmation_message']) ? $email->settings['confirmation_message'] : t('You have been successfully subscribed.'),
  );

  $form['ym_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Yesmail Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // @TODO: Use the masters API
  // e.g. https://api.yesmail.com/v2/masters/{id}
  // ref: https://developer.yesmail.com/get-mastersid
  $form['ym_config']['mmid'] = array(
    '#type' => 'textfield',
    '#title' => t('Yesmail Template ID'),
    '#description' => t('Enter template ID (MMID) for the message that should be sent to users who fill out this form.'),
    '#default_value' => isset($email->mmid) ? $email->mmid : '',
    '#required' => TRUE,
  );
  $form['ym_config']['transactional'] = array(
    '#type' => 'checkbox',
    '#title' => t('Transactional email form'),
    '#description' => t('A flag that specifies whether the email is transactional (to be sent regardless of subscription status) or not.'),
    '#default_value' => isset($email->settings['transactional']) ? $email->settings['transactional'] : '',
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 9,
  );
  if ($form_state['op'] == 'edit') {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#access' => isset($email),
      '#submit' => array('yesmail_email_delete_submit'),
      '#weight' => 10,
    );
  }
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/services/yesmail/email',
    '#weight' => 10,
  );

  return $form;
}

/**
 * Submit handler for yesmail_email_form().
 */
function yesmail_email_form_submit($form, &$form_state) {
  if (isset($form_state['email'])) {
    $email = $form_state['email'];
  }
  else {
    $email = yesmail_email_create();
  }
  $email->title = $form_state['values']['title'];
  $email->name = $form_state['values']['name'];
  $email->mode = $form_state['values']['mode'];
  $email->mmid = $form_state['values']['mmid'];
  $email->settings = $form_state['values']['settings'];
  $email->settings['description'] = $form_state['values']['description'];
  $email->settings['transactional'] = $form_state['values']['transactional'];
  if ($email->save()) {
    drupal_set_message(t('Email form @name has been saved.',
      array('@name' => $email->name)));
    $form_state['redirect'] = 'admin/config/services/yesmail/email';
  }
  else {
    drupal_set_message(t('There has been an error saving your email form.'), 'error');
  }
}

/**
 * Email deletion form.
 */
function yesmail_email_email_delete_form($form, &$form_state, $email) {
  $form_state['email'] = $email;
  return confirm_form($form,
    t('Are you sure you want to delete the email form %name?', array('%name' => $email->label())),
    'admin/config/services/yesmail/email/' . $email->identifier() . '/edit',
    t('This action cannot be undone.'),
    t('Delete Email form'));
}

/**
 * Submit function for the delete button on the email overview and edit forms.
 */
function yesmail_email_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/services/yesmail/email/' . $form_state['email']->identifier() . '/delete';
}

/**
 * Submit handler for yesmail_email_email_delete_form().
 */
function yesmail_email_email_delete_form_submit($form, &$form_state) {
  $email = $form_state['email'];
  yesmail_email_delete_multiple(array($email->identifier()));
  drupal_set_message(t('%name has been deleted.', array('%name' => $email->label())));
  $form_state['redirect'] = 'admin/config/services/yesmail/email';
}

/**
 * Create a new Yesmail Email object.
 *
 * @param array $values
 *   Associative array of values.
 *
 * @return YesmailEmail
 *   New YesmailEmail entity.
 */
function yesmail_email_create(array $values = array()) {
  return entity_get_controller('yesmail_email')->create($values);
}
