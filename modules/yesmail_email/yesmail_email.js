/**
 * @file
 * JavaScript behaviors for the front-end display of yesmail_email forms.
 */

(function ($) {

  "use strict";

  Drupal.behaviors.yesmail_email = Drupal.behaviors.yesmail_email || {};
  Drupal.behaviors.yesmail_email.attach = function (context) {
    if (Drupal.settings.yesmail_email.form_id) {
      Drupal.yesmail_email.checkEmail(Drupal.settings.yesmail_email.form_id);
    }
  };

  Drupal.yesmail_email = Drupal.yesmail_email || {};

  Drupal.yesmail_email.checkEmail = function (form_id) {
    var form = $('#' + form_id);
    form.validate({
      rules: {
        email: {
          required: true,
          email: true
        }
      },
      messages: {
        email: {
          required: "Email address required",
          email: "Email address invalid"
        }
      },
      errorPlacement: function (error, element) {
        error.insertAfter(element);
      }
    });

    $('#' + form_id + ' input').on('keyup', function () {
      if (form.valid()) {
        form.find('input[type=submit]').prop('disabled', false);
      }
      else {
        form.find('input[type=submit]').prop('disabled', 'disabled');
      }
    });
  };
})(jQuery);
