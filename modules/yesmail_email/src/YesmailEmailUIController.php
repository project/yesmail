<?php

/**
 * @file
 * Class file for Yesmail Email UI Controller.
 */

namespace Drupal\yesmail_email;

/**
 * Override EntityDefaultUIController to customize our menu items.
 */
class YesmailEmailUIController extends \EntityDefaultUIController {

  /**
   * Get the name of the class.
   *
   * @return string
   *   The name of the class.
   */
  static public function getClassName() {
    return get_called_class();
  }

  /**
   * Overrides parent::hook_menu().
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['title'] = t('Email Forms');
    $items[$this->path]['description'] = t('Manage Yesmail Email blocks.');
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    $items[$this->path]['weight'] = 10;
    $items[$this->path]['access callback'] = 'yesmail_email_entity_access';

    return $items;
  }

  /**
   * Overrides parent::overviewTable().
   */
  public function overviewTable($conditions = array()) {
    $render = parent::overviewTable($conditions);
    foreach ($render['#rows'] as &$row) {
      $email = $row[0]['data']['#url']['options']['entity'];
      $modes = NULL;
      switch ($email->mode) {
        case YESMAIL_EMAIL_BLOCK:
          $modes = l(t('Block'), 'admin/structure/block');
          break;
      }
      $new_row = array();
      // Put the label column data first:
      $new_row[] = array_shift($row);
      // Now our custom columns:
      $new_row[] = $modes;
      // Now tack on the remaining built-in rows:
      $row = array_merge($new_row, $row);
    }
    $new_header[] = array_shift($render['#header']);
    $new_header[] = t('Display Mode(s)');
    $render['#header'] = array_merge($new_header, $render['#header']);
    return $render;
  }

}
