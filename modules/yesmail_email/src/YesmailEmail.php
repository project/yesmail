<?php

/**
 * @file
 * Yesmail Email entity class.
 */

namespace Drupal\yesmail_email;

/**
 * Override Entity.
 */
class YesmailEmail extends \Entity {

  /**
   * Get the name of the class.
   *
   * @return string
   *   The name of the class.
   */
  static public function getClassName() {
    return get_called_class();
  }

  /**
   * The yesmail_email entity ID.
   *
   * @var int
   */
  public $yid;

  /**
   * The yesmail_email entity machine name.
   *
   * @var string
   */
  public $name;

  /**
   * The display mode for yesmail_email entities.
   *
   * @var int
   */
  public $mode;

  /**
   * The human readable title of yesmail_email entities.
   *
   * @var string
   */
  public $title;

  /**
   * The template ID of a Yesmail email template.
   *
   * @var int
   */
  public $mmid;

  /**
   * Settings for a yesmail_email entity.
   *
   * @var array
   */
  public $settings;

  /**
   * Override parent constructor to set the entity type.
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'yesmail_email');
  }

  /**
   * Return a label for a email form.
   */
  public function label() {
    return $this->title;
  }

  /**
   * Overrides \Entity\Entity::uri().
   */
  public function uri() {
    return array(
      'path' => 'admin/config/services/yesmail/email/manage/' . $this->name,
      'options' => array(
        'entity_type' => $this->entityType,
        'entity' => $this,
      ),
    );
  }

}
